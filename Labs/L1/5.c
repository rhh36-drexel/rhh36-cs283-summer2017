#include <stdlib.h>
#include <stdio.h>
#include "5.h"

//Array initializer
int* init(int size) {
	int* ptr = (int*)malloc(size * sizeof(int));
	return ptr;
}

void add(int* array, int entry, int* size, int* maxsize) {
	int* oldptr = array;
	//resize if full
	if (*maxsize <= *size) {
		(*maxsize) = (*maxsize) * 2;
		//or (*maxsize)=(*maxsize)+1;
		array = (int*)realloc(array, ((*maxsize) * sizeof(int)));
	}
	array[(*size)] = entry;
	*size = (*size) + 1;
}

void prob5_run() {
	int maxsize = 1;
	int size = 0;
	int* ptr = init(maxsize);

	for (int i = 0; i < 10000; i++) {
		add(ptr, i, &size, &maxsize);
	}

	free(ptr);
}