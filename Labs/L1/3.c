#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "3.h"

// Write a function sort() that takes in an int* a and int size, and sorts the array using pointer arithmetic.
// I used a bubblesort algorithm
void sort(int* a, int size) {
	int temp;
	for (int x = 0; x < (size-1); x++) {
		for (int y = 0; y < (size - x - 1); y++) {
			if (a[y] > a[y + 1]) {
				temp = a[y];
				a[y] = a[y + 1];
				a[y + 1] = temp;
			}
		}
	}
}