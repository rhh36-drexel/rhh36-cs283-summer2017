#include <stdio.h>
#include <stdlib.h>
#include "1.h"

void intArray() {
	int* x = (int*) malloc(10 * sizeof(int));
	int i;
	for (i = 0; i < 10; i++) {
		x[i] = i;
		printf("%d", x[i]);
		printf("%s", "\n");
	}
	free(x);
}