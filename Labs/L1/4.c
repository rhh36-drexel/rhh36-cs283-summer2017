#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "4.h"

void NodeSwap(struct ListNode* a, struct ListNode* b) {
	struct ListNode* aPrev = a->prev;
	struct ListNode* bPrev = b->prev;
	struct ListNode* aNext = a->next;
	struct ListNode* bNext = b->next;

	if (aNext == b) {
		a->next = bNext;
		b->next = a;
		a->prev = b;
		b->prev = aPrev;

		if (aPrev != NULL) {
			aPrev->next = b;
		}
		if (bNext != NULL) {
			bNext->prev = a;
		}
	}
	else if (bNext == a) {
		b->next = aNext;
		a->next = b;
		b->prev = a;
		a->prev = bPrev;

		if (bPrev != NULL) {
			bPrev->next = a;
		}
		if (aNext != NULL) {
			aNext->prev = b;
		}
	}
	else {
		a->next = bNext;
		b->next = aNext;
		a->prev = bPrev;
		b->prev = aPrev;

		if (aPrev != NULL) {
			aPrev->next = b;
		}
		if (bPrev != NULL) {
			bPrev->next = a;
		}
		if (aNext != NULL) {
			aNext->prev = b;
		}
		if (bNext != NULL) {
			bNext->prev = a;
		}
	}
}

void printlist(struct ListNode* start) {
	struct ListNode* current = start;
	int next = 1;
	int i = 0;
	while (next) {
		if (current->next == NULL) {
			next = 0;
			printf("a[%d]=%d\n", i, current->value);
		}
		else {
			printf("a[%d]=%d, ", i, current->value);
			current = current->next;
			i++;
		}
	}
}

void sort_ll(struct ListNode* first) {
	printlist(first);
	struct ListNode* head = (struct ListNode*) malloc(sizeof(struct ListNode));
	head->next = first;

	struct ListNode* x = head;
	while (x->next != NULL) {
		x = x->next;
	}
	struct ListNode* last = x;
	x = head;
	while (x->next != last) {
		x = x->next;
		struct ListNode* y = last;
		while (y != x) {
				if (y->value < y->prev->value) {
					NodeSwap(y, y->prev);
				}
			y = y->prev;
		}
	}
}

//int main() {
//	struct ListNode a;
//	struct ListNode b;
//	struct ListNode c;
//	struct ListNode d;
//	struct ListNode e;
//	struct ListNode f;
//	a.value = 1;
//	b.value = 4;
//	c.value = 3;
//	d.value = 5;
//	e.value = 2;
//	f.value = 6;
//	a.next = &b;
//	b.next = &c;
//	c.next = &d;
//	d.next = &e;
//	e.next = &f;
//	f.next = NULL;
//	a.prev = NULL;
//	b.prev = &a;
//	c.prev = &b;
//	d.prev = &c;
//	e.prev = &d;
//	f.prev = &e;
//
//	printlist(&a);
//	sort_ll(&a);
//	printlist(&a);
//
//	getchar();
//}