#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "1.h"
#include "2.h"
#include "3.h"
#include "4.h"
#include "5.h"

int main() {
	printf("%s", "This program will demo our solutions to problems 1-5.\n");

	printf("%s", "---------\nProblem 1\n---------\n");
	intArray();

	printf("%s", "\nPress any key to see the next demo.\n");
	getchar();

	printf("%s", "---------\nProblem 2\n---------\n");
	charArray();

	printf("%s", "\nPress any key to see the next demo.\n");
	getchar();

	printf("%s", "---------\nProblem 3\n---------\n");
	printf("%s", "For now I supply array.\n");
	int* prob3_arr[7] = { 1, 0, -3, 4, 3, 2, 1 };
	int prob3_size = 7;
	printf("%s \n", "Unsorted list:");
	for (int i = 0; i < prob3_size; i++) {
		printf("%d ", prob3_arr[i]);
	}
	printf("\n");
	sort(prob3_arr, prob3_size);
	printf("%s\n", "Sorted list:");
	for (int i = 0; i < prob3_size; i++) {
		printf("%d ", prob3_arr[i]);
	}

	printf("%s", "\n\nPress any key to see the next demo.\n");
	getchar();

	printf("%s", "---------\nProblem 4\n---------\n");
	struct ListNode a;
	struct ListNode b;
	struct ListNode c;
	struct ListNode d;
	struct ListNode e;
	struct ListNode f;
	a.value = 1;
	b.value = 2;
	c.value = 3;
	d.value = 5;
	e.value = 4;
	f.value = 6;
	a.next = &b;
	b.next = &c;
	c.next = &d;
	d.next = &e;
	e.next = &f;
	f.next = NULL;
	a.prev = NULL;
	b.prev = &a;
	c.prev = &b;
	d.prev = &c;
	e.prev = &d;
	f.prev = &e;

	printlist(&a);
	sort_ll(&a);
	printlist(&a);

	printf("%s", "\n\nPress any key to see the next demo.\n");
	getchar();

	printf("%s", "---------\nProblem 5\n---------\n");
	//prob5_run();
	//We couldn't get Problem 5 to work within main.c. It still works as a standalone program.

	printf("%s", "There's no output for problem 5. That's all.\n");
	getchar();

	return 0;
}