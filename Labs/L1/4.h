#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ListNode {
	int value;
	struct ListNode* next;
	struct ListNode* prev;
};

void NodeSwap(struct ListNode* a, struct ListNode* b);
void printlist(struct ListNode* start);
void sort_ll(struct ListNode* first);