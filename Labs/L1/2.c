#include <stdio.h>
#include <stdlib.h>
#include "2.h"

void charArray() {
	char** ptr = (char**)malloc(10 * sizeof(char*));
	char string[15] = { 't','u','r','i','n','g',' ','m','a','c','h','i','n','e','\0' };

	for (int i = 0; i < 10; i++) {
		ptr[i] = (char*)malloc(15 * sizeof(char));
		for (int j = 0; j < 15; j++) {
			ptr[i][j] = string[j];
		}
		printf("String at ptr[%d] = %s\n", i, ptr[i]);
	}

	//free subpointers first
	for (int i = 0; i < 10; i++) {
		free(ptr[i]);
	}

	free(ptr);
}