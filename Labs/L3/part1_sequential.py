#!/usr/bin/env python

# function to convert csv data to list of dictionaries
def csv_to_dicts(csv):
	data = []
	
	with open(csv, 'r') as data_file:
		
		index = 0
		
		
		for l in data_file:
			l = l.split(',')
			l = l[1:] # first element of line isn't identified
			if not index == 0: # we don't want the header line
				d = {}
				headers = ['Year','Month','DayofMonth','DayOfWeek','DepTime','CRSDepTime','ArrTime','CRSArrTime','UniqueCarrier','FlightNum','TailNum','ActualElapsedTime','CRSElapsedTime','AirTime','ArrDelay','DepDelay','Origin','Dest','Distance','TaxiIn','TaxiOut','Cancelled','CancellationCode','Diverted','CarrierDelay','WeatherDelay','NASDelay','SecurityDelay','LateAircraftDelay']
				for i, e in enumerate(headers):
					d[e] = l[i] # we add each element of each line to a dictionary
					
				data.append(d) # append the dictionary to the list
			index+=1
	return data

# answer a) which airports had the longest delays due to security and weather (combined)?
def sequential_a(data):
	# We'll keep a running list of [airport, totaldelaytime, n] lists, and find the average afterwards
	delay_info_lists = []
	for d in data:
		origin = d['Origin']
		
		weather_delay = d['WeatherDelay']
		security_delay = d['SecurityDelay']
		
		# for weather_delay, security_delay, we assume no value = 0 delay
		if not weather_delay:
			weather_delay = 0.0
		else:
			weather_delay = float(weather_delay)
	
		if not security_delay:
			security_delay = 0.0
		else:
			security_delay = float(security_delay)
		
		total_delay = weather_delay + security_delay
	
		# if the airport doesn't have an entry, we create a new one with n=1
		if origin not in [x[0] for x in delay_info_lists]:
			delay_info_lists.append([origin, total_delay, 1])
		# otherwise we add the delay time to the running sum and increment n
		else:
			for dil in delay_info_lists:
				if origin == dil[0]:
					dil[1] += total_delay
					dil[2] += 1
	
	# now we calculate the average delay
	avg_delays = []
	for dil in delay_info_lists:
		avg_delays.append([dil[0], dil[1]/dil[2]])
	
	# we loop over once more to find the max delayed airport
	max_delay = avg_delays[0]
	for ad in avg_delays:
		if ad[1] > max_delay[1]:
			max_delay = ad
	
	print 'A\nAirport w/highest avg delay time: ' + max_delay[0] + '\nAvg delay time: ' + str(max_delay[1]) + '\n\n'

# answer b) Which carriers had the longest carrier delays?
def sequential_b(data):
	# similar to a), we'll keep a running tally of carrier delays and average afterwards
	carrier_delay_lists = []
	for d in data:
		carrier = d['UniqueCarrier']
		carrier_delay = d['CarrierDelay']
		
		if not carrier_delay:
			carrier_delay = 0.0
		else:
			carrier_delay = float(carrier_delay)
			
		# same as in a) - we add an entry to cdl if carrier isn't already in there,
		# otherwise we increment
		if carrier not in [x[0] for x in carrier_delay_lists]:
			carrier_delay_lists.append([carrier, carrier_delay, 1])
		else:
			for cdl in carrier_delay_lists:
				if carrier == cdl[0]:
					cdl[1] += carrier_delay
					cdl[2] += 1
					
	# now we calculate average
	avg_delays = []
	for cdl in carrier_delay_lists:
		avg_delays.append([cdl[0], cdl[1]/cdl[2]])
		
	# and find max
	max_delay = avg_delays[0]
	for ad in avg_delays:
		if ad[1] > max_delay[1]:
			max_delay = ad
			
	print 'B\nCarrier w/highest avg delay time: ' + max_delay[0] + '\nAvg delay time: ' + str(max_delay[1]) + '\n\n'

# answer c) What was the average total late aircraft delay for each airport?
def sequential_c(data):
	# keep a running list of [airport, lateaircraftdelay] pairs
	late_aircraft_delays = []
	for d in data:
		airport = d['Origin']
		late_aircraft_delay = d['LateAircraftDelay'].strip() # last entry in csv, get rid of trailing chars
		
		
		if not late_aircraft_delay:
			late_aircraft_delay = 0.0
		else:
			late_aircraft_delay = float(late_aircraft_delay)
		
		# if there's no entry in our running list, create one, else increment
		if airport not in [x[0] for x in late_aircraft_delays]:
			late_aircraft_delays.append([airport, late_aircraft_delay, 1])
		else:
			for lad in late_aircraft_delays:
				if airport == lad[0]:
					lad[1] += late_aircraft_delay
					lad[2] += 1
					
	# calculate average
	avg_delays = []
	for lad in late_aircraft_delays:
		avg_delays.append([lad[0], lad[1]/lad[2]])
					
	# print results
	print 'C\nAirport | Avg Late Aircraft Delay'
	for ad in avg_delays:
		print '    ' + ad[0] + ' | ' + str(ad[1])
	print '\n\n'

# answer d) What was the average total late aircraft delay for each carrier?
def sequential_d(data):
	# this is the exact same as c), except by carrier
	# keep a running list of [carrier, lateaircraftdelay] pairs
	late_aircraft_delays = []
	for d in data:
		carrier = d['UniqueCarrier']
		late_aircraft_delay = d['LateAircraftDelay'].strip() # last entry in csv, get rid of trailing chars
		
		
		if not late_aircraft_delay:
			late_aircraft_delay = 0.0
		else:
			late_aircraft_delay = float(late_aircraft_delay)
		
		# if there's no entry in our running list, create one, else increment
		if carrier not in [x[0] for x in late_aircraft_delays]:
			late_aircraft_delays.append([carrier, late_aircraft_delay, 1])
		else:
			for lad in late_aircraft_delays:
				if carrier == lad[0]:
					lad[1] += late_aircraft_delay
					lad[2] += 1
					
	# calculate average
	avg_delays = []
	for lad in late_aircraft_delays:
		avg_delays.append([lad[0], lad[1]/lad[2]])
					
	# print results
	print 'D\nCarrier | Avg Late Aircraft Delay'
	for ad in avg_delays:
		print '     ' + ad[0] + ' | ' + str(ad[1])
	print '\n\n'

# this function computes the solutions with simple sequential algorithms
# a) which airports had the longest delays due to security and weather (combined)? 
# b) Which carriers had the longest carrier delays? 
# c) What was the average total late aircraft delay for each airport? 
# d) What was the average total late aircraft delay for each carrier?
def sequential_analysis(csv):
	data = csv_to_dicts(csv)
	sequential_a(data)
	sequential_b(data)
	sequential_c(data)
	sequential_d(data)