#!/usr/bin/env python

from part1_sequential import sequential_analysis
from mapreduce_v2 import mapreduce_analysis
import time
import sys

clock_time = time.clock()
sequential_analysis('./DelayedFlights.csv')
sequential_time = time.clock() - clock_time
print "Sequential time: " + str(sequential_time)
print "GOT HERE"


nodes = int(sys.argv[1].split('=')[1]) # input is 'NODES=8', just get the integer
clock_time = time.clock()
mapreduce_analysis('./DelayedFlights.csv', nodes)
mapreduce_time = time.clock() - clock_time
print "Mapreduced time: " + str(mapreduce_time)