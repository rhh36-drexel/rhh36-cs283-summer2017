#!/usr/bin/env python

import threading
import sys

# split input data into n chunks
def split_data(csv, n):
	# to start, we want a list of n empty lists
	chunks = []
	for i in range(n):
		chunks.append([])
	
	index = 0
	with open(csv, 'r') as f:
		for l in f:
			if not index == 0: # again, we don't want first line
				for i in range(n):
					if index % n == i: # create n lists of equal len
						chunks[i].append(l)
			index += 1
	
	return chunks

# a) which airports had the longest delays due to security and weather (combined)?
def a(csv, n):
	# given a line of data, return [airport, delay] list
	def get_delay_info(l):
		l = l.split(',')
		l = l[1:]
		airport = l[16]
		weather = l[25]
		security = l[27]
		
		# parse weather, security to floats
		if not weather:
			weather = 0.0
		weather = float(weather)
		
		if not security:
			security = 0.0
		security = float(security)
				
		return [airport, weather+security]
	
	# given a list of lines return a list of delay info lists
	def total_delay_info(l):
		total = []
		for line in l:
			total.append(get_delay_info(line))
		return total
	
	master_dict = {}
	# take [airport, delay] info and add to dict or create new entry
	def to_master_dict(delay_info):
		if delay_info[0] not in master_dict.keys():
			master_dict[delay_info[0]] = [delay_info[1], 1]
		else:
			master_dict[delay_info[0]][0] += delay_info[1]
			master_dict[delay_info[0]][1] += 1
	
	# we'll wrap all the thread activities into one function
	def lines_to_master_dict(lines):
		tdi = total_delay_info(lines)
		for l in tdi:
			to_master_dict(l)
	
	
	
	# first split into n chunks
	chunks = split_data(csv, n)
	
	
	# create a thread for each chunk and run the wrapper function
	for i in range(n):
		t = threading.Thread(target = lines_to_master_dict, args=(chunks[i],))
		t.start()
		t.join()
	
	# now that master_dict is populated with [totaldelay, ndelays]
	# entries, we can find averages and return the greatest
	first_key = master_dict.keys()[0]
	max_delay = [first_key, master_dict[first_key][0]/master_dict[first_key][1]]
	
	for k in master_dict.keys():
		if master_dict[k][0]/master_dict[k][1] > max_delay[1]:
			max_delay = [k, master_dict[k][0]/master_dict[k][1]]
	
	print "Airport with max delay: " + max_delay[0]
	print "Delay time: " + str(max_delay[1])
	

# b) Which carriers had the longest carrier delays?
def b(csv, n):
	# return [carrier, carrierdelay] list given a line
	def get_info(l):
		l = l.split(',')
		l = l[1:]
		carrier = l[8]
		carrierdelay = l[24]
		
		#parse carrierdelay to float
		if not carrierdelay:
			carrierdelay = 0.0
		carrierdelay = float(carrierdelay)
		
		return [carrier, carrierdelay]
		
	
	# given list of lines generate list of [c, cd] lists
	def total_carrier_lists(l):
		total = []
		for line in l:
			total.append(get_info(line))
		return total

			
	master_dict = {}
	# write [c, cd] list to master dict
	def to_master_dict(carrier_info):
		if carrier_info[0] not in master_dict.keys():
			master_dict[carrier_info[0]] = [carrier_info[1], 1]
		else:
			master_dict[carrier_info[0]][0] += carrier_info[1]
			master_dict[carrier_info[0]][1] += 1
			
	# wrap into one function
	def lines_to_master_dict(lines):
		tcl = total_carrier_lists(lines)
		for l in tcl:
			to_master_dict(l)
	
	chunks = split_data(csv, n)
		
	# thread stuff
	for i in range(n):
		t = threading.Thread(target = lines_to_master_dict, args=(chunks[i],))
		t.start()
		t.join()
	
	# find averages, print max
	first_key = master_dict.keys()[0]
	max_delay = [first_key, master_dict[first_key][0]/master_dict[first_key][1]]
	
	for k in master_dict.keys():
		if master_dict[k][0]/master_dict[k][1] > max_delay[1]:
			max_delay = [k, master_dict[k][0]/master_dict[k][1]]
			
	print "Carrier with max delay: " + max_delay[0]
	print "Delay time: " + str(max_delay[1])

# c) What was the average total late aircraft delay for each airport?	
def c(csv, n):
	# given a line, return [airport, lad]
	def get_info(l):
		l = l.split(',')
		l = l[1:]
		airport = l[16]
		lad = l[-1].strip()
		
		if not lad:
			lad = 0.0
		lad = float(lad)
		
		return [airport, lad]
		
	# given list of lines generate list of [airport, lad] lists
	def total_lad_lists(l):
		total = []
		for line in l:
			total.append(get_info(line))
		return total
	
	master_dict = {}
	
	# write [airport, lad] list to master dict
	def to_master_dict(lad_info):
		if lad_info[0] not in master_dict.keys():
			master_dict[lad_info[0]] = [lad_info[1], 1]
		else:
			master_dict[lad_info[0]][0] += lad_info[1]
			master_dict[lad_info[0]][1] += 1
	
	# wrap above into one function
	def lines_to_master_dict(lines):
		tll = total_lad_lists(lines)
		for l in tll:
			to_master_dict(l)
	
	chunks = split_data(csv, n)
	
	# thread stuff
	for i in range(n):
		t = threading.Thread(target = lines_to_master_dict, args=(chunks[i],))
		t.start()
		t.join()
	
	# average and print results
	print 'Airport | LAD'
	for k in master_dict.keys():
		print '    ' + k + ' | ' + str(master_dict[k][0]/master_dict[k][1])

# d) What was the average total late aircraft delay for each carrier?		
def d(csv, n):
	# given a line, return [carrier, lad]
	def get_info(l):
		l = l.split(',')
		l = l[1:]
		carrier = l[8]
		lad = l[-1].strip()
		
		if not lad:
			lad = 0.0
		lad = float(lad)
		
		return [carrier, lad]
		
	# given list of lines generate list of [carrier, lad] lists
	def total_lad_lists(l):
		total = []
		for line in l:
			total.append(get_info(line))
		return total
	
	master_dict = {}
	
	# write [carrier, lad] list to master dict
	def to_master_dict(lad_info):
		if lad_info[0] not in master_dict.keys():
			master_dict[lad_info[0]] = [lad_info[1], 1]
		else:
			master_dict[lad_info[0]][0] += lad_info[1]
			master_dict[lad_info[0]][1] += 1
	
	# wrap above into one function
	def lines_to_master_dict(lines):
		tll = total_lad_lists(lines)
		for l in tll:
			to_master_dict(l)
	
	chunks = split_data(csv, n)
	
	# thread stuff
	for i in range(n):
		t = threading.Thread(target = lines_to_master_dict, args=(chunks[i],))
		t.start()
		t.join()
	
	# average and print results
	print 'Carrier | LAD'
	for k in master_dict.keys():
		print '     ' + k + ' | ' + str(master_dict[k][0]/master_dict[k][1])
	
def mapreduce_analysis(csv, n):
	print 'A'
	a(csv, n)
	print 'B'
	b(csv, n)
	print 'C'
	c(csv, n)
	print 'D'
	d(csv, n)
