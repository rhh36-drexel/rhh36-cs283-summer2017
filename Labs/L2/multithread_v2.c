#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/*
	Next step:
	Now, create a new version of your program, and using mutex locks (lock inside the for loop), fix the code, and re-run it 10 times.  Again, time the runs, and give the average runtime.  As before, give the final output of the counter (this time, it really should be 100000).  How much slower was this program than the one without mutex locks?  
*/

volatile int i = 0; // var to be incremented
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// thread routine: increment 1000 times
void* thread_increment(void* arg) {
	for (int a = 0; a != 1000; a++) {
		pthread_mutex_lock(&mutex);
		i++;
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

int main() {
	pthread_mutex_t threads[100]; // create our threads
	
	for (int a = 0; a != 100; a++) {
		pthread_create(&threads[a], NULL, thread_increment, NULL);
	}
	
	
	
	printf("Final value: %d\n", i);
	pthread_exit(NULL);
	return 0;
}