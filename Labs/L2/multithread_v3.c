#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/*
	Final step:
	Now, in a new version of the program, move the locks so that they lock and unlock only once per thread (outside the for loop).  Verify the correct value is still obtained, and find the average runtime over 10 runs again.  What do you observe as compared to the other two versions?
*/

volatile int i = 0; // var to be incremented
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// thread routine: increment 1000 times
void* thread_increment(void* arg) {
	pthread_mutex_lock(&mutex); // we just moved the lock and unlock outside of the for loop
	for (int a = 0; a != 1000; a++) {
		
		i++;
		
	}
	pthread_mutex_unlock(&mutex);
	return NULL;
}

int main() {
	pthread_mutex_t threads[100]; // create our threads
	
	for (int a = 0; a != 100; a++) {
		pthread_create(&threads[a], NULL, thread_increment, NULL);
	}
	
	
	
	printf("Final value: %d\n", i);
	pthread_exit(NULL);
	return 0;
}