#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/*
	First step of lab:
		- one threaded function invoked via 100 pthreads
		- increment shared variable (initialized to 0, volatile) 1000 times each
*/

volatile int i = 0; // var to be incremented

// thread routine: increment 1000 times
void* thread_increment(void* arg) {
	for (int a = 0; a != 1000; a++) {
		i++;
	}
	return NULL;
}

int main() {
	pthread_t threads[100]; // create our threads
	
	for (int a = 0; a != 100; a++) {
		pthread_create(&threads[a], NULL, thread_increment, NULL);
		pthread_join(threads[a], NULL);
	}
	
	
	
	printf("Final value: %d\n", i);
	pthread_exit(NULL);
	return 0;
}