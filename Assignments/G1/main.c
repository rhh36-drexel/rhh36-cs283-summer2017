#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main() {
	
	// This section of code opens up the database directory.
	printf("\nEnter the name of an existing database, or a new one you'd like to create.\nExisting databases:\n");
	
	DIR* dp;
	struct dirent *ep;
	dp = opendir("./DB");
	
	if (dp!=NULL) {
		while (ep = readdir(dp)) {
			puts(ep->d_name);
		}
		(void) closedir(dp);
	}
	else {
		perror("Couldn't open directory");
	}
	
	
	// Now check if database exists in directory. If it doesn't, create it.
	printf(">>  ");
	char db[100] = "\0";
	scanf("%s", db);
	// Full path to database 'd' will be "./DB/d". Let's put this in a variable.
	char* pathtodir = malloc(strlen("./DB/") + strlen(db) + 2); // We add two bytes for terminating characters
	strcpy(pathtodir, "./DB/");
	strcat(pathtodir, db); // now pathtodir contains what we want
	
	// Now we check if pathtodir is a directory, and create it if necessary.
	struct stat st = {0};
	if (stat(pathtodir, &st) == -1) {
		mkdir(pathtodir, 0700);
	}
	printf("Active database: %s\n", db);
	
	
	/* Here we start the loop to take and execute commands until exit. We need to initialize a few variables:
	Setting the 'running' var to false will exit the loop.
	We use the 'input' var to determine whether to execute a sql command, view commands, select another database, or exit the program.
	The 'operation' var takes the sql operation if the user enters one. We use the 'token' var to split the operation word-by-word.
	*/
	bool running = true;
	char cmd;
	char input[1] = "\0";
	char operation[100] = "\0";
	char* token;
	
	while (running) {
		printf("%s", "Press 'c' to enter a command. Press 'l' to view list of commands.  Press 'e' to exit.\n>>  ");
		scanf("%s", input);
		cmd = input[0];
		if (cmd == 'c') {
			printf("\t  >> ");
			
			// Here, we use fgets() instead of scanf() to more easily allow for whitespace in the command. First, we use getchar() to remove the last '\n' from the input stream.
			cmd = getchar();
			fgets(operation, 100, stdin);
			
			// Now we have the operation. We strtok to get the first word, with different logic for each:
			token = strtok(operation, " ");
			
			// Even after we get a SELECT operation, we have to check for two cases, as the JOIN command starts with SELECT
			if (strcmp(token, "SELECT") == 0) {
				// We need the name of the (first) table in either case.
				token = strtok(NULL, " ");
				if (strcmp(token, "*") == 0) {
					token = strtok(NULL, " ");
					if (strcmp(token, "FROM") == 0) {
						char* table = strtok(NULL, " ");
						token = strtok(NULL, " ");
						// Now we see if it's a normal SELECT or a JOIN (or a syntax error).
						if (strcmp(token, "WHERE") == 0) { // normal SELECT
							// Now we want the full path to the table file. It will be "./DB/dbname/tablename". Let's concatenate to get this.
							char* pathtotable = malloc(strlen(pathtodir) + strlen("/") + strlen(table) + 3); // add 3 bytes for terminating characters
							strcpy(pathtotable, pathtodir);
							strcat(pathtotable, "/");
							strcat(pathtotable, table);
							
							// Check that pathtotable is a valid file.
							if (access(pathtotable, F_OK) != -1) {
								// The next token is in the form "Field_x=Value_y".
								// We want to get both.
								token = strtok(NULL, " ");
								token = strtok(token, "\n");
								char* field = strtok(token, "=");
								char* value = strtok(NULL, "=");
								
								// Now open file for reading.
								FILE* tablefile;
								tablefile = fopen(pathtotable, "r");
								
								// Read line by line.
								char line[100];
								char line_backup[100]; // We want to keep the full line after we strtok
								int linecount = 0;
								int fieldindex = 0;
								int numfields = 0;
								while (fgets(line, sizeof(line), tablefile)) {
									strcpy(line_backup, line);
									if (linecount == 0) {
										// First, find how many fields there are, so we don't get a segmentation error when searching.
										for (int i = 0; i < strlen(line); i++) {
											if (line[i] == ',') {
												numfields++;
											}
										}	 
										numfields++; // There is one more field than the number of commas
										
										// Now that we know the number of fields, we strtok up to that many times to find the field index.
										token = strtok(line, ",");
										for (int i = 0; i < numfields-1; i++) {
											if (strcmp(token, field) == 0) {
												fieldindex = i;
											}
											token = strtok(NULL, ",");
										}
									}
									// For every other line, check if the fieldindexth field=value.
									// Print line if so.
									else {
										token = strtok(line, ",");
										int i = 0;
										while (i < fieldindex) {
											token = strtok(NULL, ",");
											i++;
										}
										if (strcmp(token, value) == 0) {
											printf("%s", line_backup);
										}
									}
									linecount++;
								}
								
								fclose(tablefile);
							}
							else {
								printf("Table does not exist.\n");
							}
							
						}
						// JOIN ISN'T 100% PERFECT BUT I'M LEAVING IT FOR NOW
						
						else if (strcmp(token, "JOIN") == 0) { // JOIN
							char* table2 = strtok(NULL, " ");
							
							// Get the full paths for both tables.
							char* pathtotable = malloc(strlen(pathtodir) + strlen("/") + strlen(table) + 3); // add 3 bytes for terminating characters
							strcpy(pathtotable, pathtodir);
							strcat(pathtotable, "/");
							strcat(pathtotable, table);
							
							char* pathtotable2 = malloc(strlen(pathtodir) + strlen("/") + strlen(table2) + 3); // add 3 bytes for terminating characters
							strcpy(pathtotable2, pathtodir);
							strcat(pathtotable2, "/");
							strcat(pathtotable2, table2);
							
							// Check that both tables are valid files.
							if ((access(pathtotable, F_OK) != -1) && (access(pathtotable2, F_OK) != -1)) {
								token = strtok(NULL, " ");
								if (strcmp(token, "ON") == 0) {
									char* field1 = strtok(NULL, " ");
									token = strtok(NULL, " ");
									if (strcmp(token, "=") == 0) {
										char* field2 = strtok(NULL, " ");
										field1 = strtok(field1, ".");
										field1 = strtok(NULL, ".");
										field2 = strtok(field2, ".");
										field2 = strtok(NULL, ".");
										
										// Now we find the indices of field1 and field2 in the tables.
										FILE* tablefile;
										tablefile = fopen(pathtotable, "r");

										FILE* table2file;
										table2file = fopen(pathtotable2, "r");
										
										// Variables we need to read table
										char line[100];
										char line_backup[100]; // We want to keep the full line after we strtok
										int linecount = 0;
										int fieldindex = 0;
										int numfields = 0;
										char* value1;
										
										// Variables we need to read table2
										char line2[100];
										char line2_backup[100]; // We want to keep the full line after we strtok
										int line2count = 0;
										int field2index = 0;
										int numfields2 = 0;
										
										while(fgets(line, sizeof(line), tablefile)) {
											strcpy(line_backup, line);
											if (linecount == 0) {
												// First, find how many fields there are, so we don't get a segmentation error when searching.
												for (int i = 0; i < strlen(line); i++) {
													if (line[i] == ',') {
														numfields++;
													}
												}	 
												numfields++; // There is one more field than the number of commas
												
												// Now that we know the number of fields, we strtok up to that many times to find the field index.
												token = strtok(line, ",");
												for (int i = 0; i < numfields-1; i++) {
													if (strcmp(token, field1) == 0) {
														fieldindex = i;
													}
													token = strtok(NULL, ",");
												}
											}
											
											else {
												token = strtok(line, ",");
												int i = 0;
												while (i < fieldindex) {
													token = strtok(NULL, ",");
													i++;
												}
												strcpy(value1, token);
												
												// For every line in table, we check for equivalent values in table2
												while(fgets(line2, sizeof(line2), table2file)) {
													strcpy(line2_backup, line2);
													if (line2count == 0) {
														for (int i = 0; i < strlen(line2); i++) {
															if (line2[i] == ',') {
																numfields2++;
															}
														}
														numfields2++;
														
														token = strtok(line2, ",");
														for (int i = 0; i < numfields2-1; i++) {
															if (strcmp(token, field2) == 0) {
																field2index = i;
															}
															token = strtok(NULL, ",");
														}
													}
													// For every line in table2, we go to field2index
													// and check if it's equal to value1.
													else {
														token = strtok(line2, ",");
														int i = 0;
														while (i < field2index) {
															token = strtok(NULL, ",");
															i++;
														}
														if (strcmp(token, value1) == 0) {
															printf("%s,", line_backup);
															printf("%s\n", line2_backup);
														}
													}
													
													
													line2count++;
												}
											}
											
											linecount++;
										}
										
										fclose(tablefile);
										fclose(table2file);
									}
									else {
										printf("Check syntax.\n");
									}
									
									
									
									
								}
								else {
									printf("Check syntax.\n");
								}
							}
							else {
								printf("At least one table could not be opened.\n");
							}
							
							
						}
						else {
							printf("Check syntax.\n");
						}
					}
					else {
						printf("Check syntax.\n");
					}
					
				}
				else {
					printf("Check syntax.\n");
				}
			}
			
			
			else if (strcmp(token, "UPDATE") == 0) {
				printf("Couldn't get UPDATE working.\n");
				// token = strtok(NULL, " ");
				// Now we want the full path to the table file. It will be "./DB/dbname/tablename". Let's concatenate to get this.
				// char* pathtotable = malloc(strlen(pathtodir) + strlen("/") + strlen(token) + 3); // add 3 bytes for terminating characters
				// strcpy(pathtotable, pathtodir);
				// strcat(pathtotable, "/");
				// strcat(pathtotable, token);
				
				// token = strtok(NULL, " ");
				// if (strcmp(token, "SET") == 0) {
					// char* data1 = strtok(NULL, " ");
					// token = strtok(NULL, " ");
					// if (strcmp(token, "WHERE") == 0) {
						// char* data2 = strtok(NULL, " ");
						// data2 = strtok(data2, "\n");
						// char* field1 = strtok(data1, "=");
						// char* value1 = strtok(NULL, "=");
						// char* field2 = strtok(data2, "=");
						// char* value2 = strtok(NULL, "=");
						
						// We will copy every line of the table file, changing values when necessary.
						// We will write to a tmp file and then copy tmp->original.
						// FILE* tablefile;
						// tablefile = fopen(pathtotable, "r");
						
						// Read line by line.
						// char line[100];
						// char line2[100];
						// char line_backup[100]; // We want to keep the full line after we strtok
						// int linecount = 0;
						// int field1index = 0;
						// int field2index = 0;
						// int numfields = 0;
						
						// while(fgets(line, sizeof(line), tablefile)) {
							// strcpy(line_backup, line);
							// if (linecount == 0) {
								// First, find how many fields there are, so we don't get a segmentation error when searching.
								// for (int i = 0; i < strlen(line); i++) {
									// if (line[i] == ',') {
										// numfields++;
									// }
								// }	 
								// numfields++; // There is one more field than the number of commas
								
								// Now that we know the number of fields, we strtok up to that many times to find the field index.
								// token = strtok(line, ",");
								// for (int i = 0; i < numfields-1; i++) {
									// if (strcmp(token, field1) == 0) {
										// field1index = i;
									// }
									// token = strtok(NULL, ",");
								// }
								// token = strtok(line2, ",");
								// for (int i = 0; i < numfields-1; i++) {
									// if (strcmp(token, field2) == 0) {
										// field2index = i;
									// }
									// token = strtok(NULL, ",");
								// }
								// printf("%d\n%d\n", field1index, field2index);
							// }
							
							// linecount++;
						// }
					// }
					// else {
						// printf("Check syntax.\n");
					// }
				// }
				// else {
					// printf("Check syntax.\n");
				// }
				
				// FILE* tablefile;
				// tablefile = fopen(pathtotable, "r");
				// if (tablefile != NULL) {
					
				// }
			}
			
			// When we INSERT into a table, we just add a CSV line to the end of the file.
			else if (strcmp(token, "INSERT") == 0) {
				token = strtok(NULL, " ");
				// Check for correct syntax, then get table name
				if (strcmp(token, "INTO") == 0) {
					token = strtok(NULL, " ");
					// Now we want the full path to the table file. It will be "./DB/dbname/tablename". Let's concatenate to get this.
					char* pathtotable = malloc(strlen(pathtodir) + strlen("/") + strlen(token) + 3); // add 3 bytes for terminating characters
					strcpy(pathtotable, pathtodir);
					strcat(pathtotable, "/");
					strcat(pathtotable, token);
					
					// Now we just append the last token (the CSV string itself) to the end of the file.
					token = strtok(NULL, " ");
					FILE* tablefile;
					tablefile = fopen(pathtotable, "a");
					if (tablefile != NULL) {
						fputs(token, tablefile);
						printf("Table updated.\n");
						fclose(tablefile);
					}
					else {
						printf("Could not open table.\n");
					}
				}
				else {
					printf("Check syntax.\n");
				}
			}
			
			// When we CREATE a new table, we write a new file that contains the fields, comma-separated, on the first line.
			else if (strcmp(token, "CREATE") == 0) {
				// Check for correct syntax, then get table name
				token = strtok(NULL, " ");
				if (strcmp(token, "TABLE") == 0) {
					token = strtok(NULL, " ");
					// Now we want the full path to the table file. It will be "./DB/dbname/tablename". Let's concatenate to get this.
					char* pathtotable = malloc(strlen(pathtodir) + strlen("/") + strlen(token) + 3); // add 3 bytes for terminating characters
					strcpy(pathtotable, pathtodir);
					strcat(pathtotable, "/");
					strcat(pathtotable, token);
					
					// We don't want to overwrite an existing table. Check if the file already exists.
					if (access(pathtotable, F_OK) != -1) {
						printf("Table %s already exists.\n", token);
					}
					else {
						// Check syntax again, then get fields from user input.
						token = strtok(NULL, " ");
						if (strcmp(token, "FIELDS") == 0) {
							token = strtok(NULL, " ");
							// Now token contains comma-separated fields.
							// Create file and write token to it.
							FILE* newtable;
							newtable = fopen(pathtotable, "w");
							if (newtable != NULL) {
								fputs(token, newtable);
								fclose(newtable);
								printf("Table created.\n");
							}
							else {
								printf("Error creating table.\n");
							}
						}
						else {
							printf("Check syntax.\n");
						}
						
					}
				}
				else {
					printf("Check syntax.\n");
				}
			}
			
			// DROPping a table, in our model, is as easy as deleting a file.
			else if (strcmp(token, "DROP") == 0) {
				token = strtok(NULL, " ");
				// Check for correct syntax, then get table name
				if (strcmp(token, "TABLE") == 0) {
					token = strtok(NULL, " ");
					token = strtok(token, "\n"); // User input has a trailing \n, this removes it
					// Now we want the full path to the table file. It will be "./DB/dbname/tablename". Let's concatenate to get this.
					char* pathtotable = malloc(strlen(pathtodir) + strlen("/") + strlen(token) + 3); // add 3 bytes for terminating characters
					strcpy(pathtotable, pathtodir);
					strcat(pathtotable, "/");
					strcat(pathtotable, token);
					
					// Now we check if pathtotable is a file, and delete it if so.
					int status = remove(pathtotable);
					if (status == 0) {
						printf("Table %s deleted.\n", token);
					}
					else {
						printf("Unable to delete table %s.\n", token);
					}
				}
				else {
					printf("Check syntax.\n");
				}
				
			}
			else {
				printf("Operation invalid. Please try again.\n");
			}
			
		}
		else if (cmd == 'l') {
			printf("Commands:\n");
			printf("SELECT * FROM TableName WHERE Field1=value\n");
			printf("UPDATE TableName SET Field1=newvalue WHERE Field2=value\n");
			printf("SELECT * FROM TableName JOIN TableName2 ON TableName.Field1 = TableName.Field2\n");
			printf("INSERT INTO TableName value1,value2,value3,value4\n");
			printf("CREATE TABLE TableName FIELDS Field1,Field2,Field3,Field4\n");
			printf("DROP TABLE TableName\n");

		}
		else if (cmd == 'e') {
			printf("Exiting program.\n");
			running = false;
		}
		else {
			printf("Command not recognized.\n");
		}
	}

	return 0;
}