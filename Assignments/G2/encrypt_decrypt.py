#!/usr/bin/env python

def encrypt(msg, pubkey):
	encrypted_msg = []
	for c in msg: # we encrypt every character in the string
		ascii_int = ord(c) # this converts char->int
		encrypted_int = ascii_int ** pubkey[0] % pubkey[1]
		encrypted_msg.append(str(encrypted_int))
		
	return ','.join(encrypted_msg)

# prikey is a tuple (d, c)
def decrypt(encrypted_msg, prikey):
	decrypted_msg = ''
	for n in encrypted_msg.split(','):
		decrypted_int = int(n) ** prikey[0] % prikey[1]
		decrypted_msg += chr(decrypted_int) # chr() converts int->char
	return decrypted_msg