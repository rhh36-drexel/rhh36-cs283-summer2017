#!/usr/bin/python

import socket
import sys
import threading
from encrypt_decrypt import encrypt, decrypt

#thread to listen to messages from server, detects if server sends termination message
def listener(s,t,key):
	while not t.isSet():
		msg = s.recv(1024)
		if (t.isSet()):
			break
		elif(msg == '.bye'):
			s.send('.bye')
			t.set()
			print "Received .bye message, press any key to terminate connection."
			break
		elif(msg == ''):
			s.send('.bye')
			t.set()
			print "Received .bye message, press any key to terminate connection."
			break
		else:
			print 'Message received: ',decrypt(msg,key)
	return
	
#thread to wait for input and send to server, detects if client enters termination message
def sender(s,t,key):
	while not t.isSet():
		msg = raw_input('Type, enter .bye to quit\n')
		if(t.isSet()):
			break
		elif (msg == '.bye'):
			s.send('.bye')
			t.set()
			print "Terminating connection."
			break
		elif (msg == ''):
			msg
		else:
			s.send(encrypt(msg,key))
	return
			
#get port number and connect to it
s = socket.socket()         # Create a socket object
#host = socket.gethostname() # Get local machine name
host = sys.argv[1]
port = int(sys.argv[2])
print 'Connecting to server at port ',port,' on host ',host,'.'
s.connect((host, port))
print 'Successfully connected.'

#get RSA info
e = int(sys.argv[3])
c = int(sys.argv[4])
d = int(sys.argv[5])
f = int (sys.argv[6])

mykey = (e,c)
theirkey = (d,f)

#initialize thread infrastructure and start them
threads = []

terminated = threading.Event()

reader = threading.Thread(target=listener, args=(s,terminated,theirkey))
sender = threading.Thread(target=sender, args=(s,terminated,mykey))

threads.append(reader)
threads.append(sender)

reader.start()
sender.start()

#wait until connection is terminated and then terminate program
reader.join()
sender.join()

print "Closing connection and exiting."
s.close
