#!/usr/bin/env python

from fractions import gcd
import sys

# simple function to determine if a number is prime
def is_prime(n):
	if n < 2:
		return False
	if n % 2 == 0:
		return False
	else:
		for i in range(3, n):
			if n % i == 0:
				return False
		return True

# get the nth prime
def nth_prime(n):
	primes = []
	i = 1
	while len(primes) < n:
		if is_prime(i):
			primes.append(i)
		i += 1
	return primes[-1] # the last prime we added

# egcd, modinv functions from stack overflow:
# https://stackoverflow.com/a/9758173
def egcd(a, b):
	if a == 0:
		return (b, 0, 1)
	else:
		g, y, x = egcd(b % a, a)
		return (g, x - (b // a) * y, y)
		
def modinv(a, m):
	g, x, y = egcd(a, m)
	if g != 1:
		print "does not exist"
	else:
		return x % m

# now, our rsa function
def gen_rsa():
	# if sys.argc() != 3:
		# print "Check argument format."
	a = sys.argv[1]
	a = int(a.split('=')[1]) # args are in the form MPRIME=x, NPRIME=y, so we just take what's after =
	ath = nth_prime(a)
	
	b = sys.argv[2]
	b = int(b.split('=')[1])
	bth = nth_prime(b)
	print str(a) + "th prime: " + str(ath) + ", " + str(b) +"th prime: " + str(bth)
	
	# I followed the assignment instructions to generate all results after this
	
	c = ath*bth
	m = (ath-1)*(bth-1)
	print "c = " + str(c)
	print "m = " + str(m)
	
	e = 0
	coprime_check = b**2
	found_coprimes = False
	while not found_coprimes:
		if gcd(coprime_check, c) == 1 and gcd(coprime_check, m) == 1:
			e = coprime_check
			found_coprimes = True
		else:
			coprime_check += 1
	
	print "e = " + str(e)
	d = modinv(e, m)
	print "d = " + str(d)
	
	public_key = (e, c)
	private_key = (d, c)
	print "public key: " + str(public_key)
	print "private key: " + str(private_key)
	
gen_rsa()