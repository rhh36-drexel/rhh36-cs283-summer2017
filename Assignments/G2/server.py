#!/usr/bin/python           # This is server.py file

import socket               # Import socket module
import thread
import threading
import sys
from encrypt_decrypt import encrypt, decrypt

threads = []

#listener accepts connection in a loop and initializes the threads to handle that connection
#loop terminated only by keyboard interrupt
def listener(s,mykey,theirkey):
	print 'Waiting for a client'
	s.listen(1)
	
	while True:
		c, addr = s.accept()
		con = threading.Thread(target=connection, args=(c,mykey,theirkey))
		threads.append(con)
		print 'Client accepted: Addr= ',addr,'.'
		con.start()
		
#starts up listener and sender threads, terminates them if connection is terminated.
def connection(c,mykey,theirkey):
	terminated = threading.Event()
	
	reader = threading.Thread(target=clientreader, args=(c,terminated,theirkey))
	sender = threading.Thread(target=clientsender, args=(c,terminated,mykey))
	
	threads.append(reader)
	threads.append(sender)
	
	reader.start()
	sender.start()
	
	reader.join()
	sender.join()
	
	print "Closing connection"
	c.close
		
#thread to receive messages, determine if client sent termination message
def clientreader(c,t,key):
	while not t.isSet():
		msg = c.recv(1024)
		if (t.isSet()):
			break
		elif(msg == '.bye'):
			c.send('.bye')
			t.set()
			break
		elif(msg == ''):
			c.send('.bye')
			t.set()
			break
		else:
			print 'Received message: ',decrypt(msg,key)
	print "Received .bye message, press any key to terminate connection"
	return
		
#thread to send messages, determine if server is sending termination message
def clientsender(c,t,key):
	while not t.isSet():
		msg = raw_input('Enter message, enter .bye to close connection\n')
		if (t.isSet()):
			break
		elif (msg == 'quit'):
			t.set()
			break
		elif (msg == '.bye'):
			c.send('.bye')
			t.set()
			break
		elif (msg == ''):
			msg
		else:
			c.send(encrypt(msg,key))
	return
	
#get RSA info
e = int(sys.argv[2])
c = int(sys.argv[3])
d = int(sys.argv[4])
f = int (sys.argv[5])

mykey = (e,c)
theirkey = (d,f)

#get port number and bind to it, pass socket to listener function to wait for connection.
port = int(sys.argv[1])

print 'Binding to port ',port,', please wait.'
s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
s.bind((host, port))        # Bind to the port
print 'Server started, hostname: ',host,' on port ',port,'.'
listener(s,mykey,theirkey)
print "Terminating server"